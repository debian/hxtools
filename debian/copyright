Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hxtools
Source: http://inai.de/projects/hxtools/

Files: *
Copyright: 2004-2022, Jan Engelhardt
License: Expat

Files: sadmin/psthreads.c
       sadmin/rpmdep.pl
       sadmin/xfs_irecover.c
       sdevel/bin2c.c
       sdevel/git-forest
       sdevel/gxxdm.cpp
       sdevel/peicon.c
       sdevel/sourcefuncsize
       sdevel/spec-beautifier
       smath/graph-fanout.c
       smath/graph-lchain.c
       smm/bsvplay.c
       smm/pcmmix.c
       smm/pcspkr_pcm.c
       smm/qplay.c
       suser/sysinfo.c
       suser/tailhex.c
       suser/wktimer
Copyright: 2002-2020, Jan Engelhardt
License: GPL-2+

Files: Makefile.in
       data/Makefile.in
       doc/Makefile.in
       kbd/Makefile.in
       sadmin/Makefile.in
       sdevel/Makefile.in
       smath/Makefile.in
       smm/Makefile.in
       suser/Makefile.in
Copyright: 1994-2021, Free Software Foundation, Inc
License: FSFULLR

Files: kbd/uefi.psf
Copyright: Intel
License: BSD-2-Clause-Patent

Files: build-aux/compile
       build-aux/depcomp
       build-aux/missing
Copyright: 1996-2021, Free Software Foundation, Inc
License: GPL-2+ with AutoConf exception

Files: data/ru-phonde.mim
       sadmin/fd0ssh.c
Copyright: 2008, Jan Engelhardt
License: LGPL-2.1+

Files: aclocal.m4
Copyright: 2012-2015, Dan Nicholson <dbn.lists@gmail.com>
           1996-2021, Free Software Foundation, Inc
           2004, Scott James Remnant <scott@netsplit.com>
License: FSFULLR or GPL-2+ with AutoConf exception

Files: configure
Copyright: 1992-2021, Free Software Foundation
           2021, Free Software Foundation, Inc
License: FSFUL

Files: debian/*
Copyright: 2016, Jochen Sprickerhof <jspricke@debian.org>
License: GPL-2+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-2-Clause-Patent
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 Subject to the terms and conditions of this license, each copyright holder
 and contributor hereby grants to those receiving rights under this license
 a perpetual, worldwide, non-exclusive, no-charge, royalty-free,
 irrevocable (except for failure to satisfy the conditions of this license)
 patent license to make, have made, use, offer to sell, sell, import, and
 otherwise transfer this software, where such license applies only to those
 patent claims, already acquired or hereafter acquired, licensable by such
 copyright holder or contributor that are necessarily infringed by:
 .
 (a) their Contribution(s) (the licensed copyrights of copyright holders
     and non-copyrightable additions of contributors, in source or binary
     form) alone; or
 .
 (b) combination of their Contribution(s) with the work of authorship to
     which such Contribution(s) was added by such copyright holder or
     contributor, if, at the time the Contribution is added, such addition
     causes such combination to be necessarily infringed. The patent license
     shall not apply to any other combinations which include the
     Contribution.
 .
 Except as expressly stated above, no rights or licenses from any copyright
 holder or contributor is granted under this license, whether expressly, by
 implication, estoppel or otherwise.
 .
 DISCLAIMER
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: FSFUL
 This configure script is free software;
 the Free Software Foundation gives unlimited permission
 to copy, distribute and modify it.

License: FSFULLR
 This file is free software;
 the Free Software Foundation gives unlimited permission
 to copy and/or distribute it,
 with or without modifications,
 as long as this notice is preserved.

License: GPL-2+
 See /usr/share/common-licenses/GPL-2.

License: GPL-2+ with AutoConf exception
 As a special exception to the GNU General Public License,
 if you distribute this file as part of a program
 that contains a configuration script generated by Autoconf,
 you may include it
 under the same distribution terms
 that you use for the rest of that program.

License: LGPL-2.1+
 See /usr/share/common-licenses/LGPL-2.1.
